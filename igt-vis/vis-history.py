#!/usr/bin/env python3
#
# piglit results.json visualizer over history and hosts
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import os
import argparse

from mako.lookup import TemplateLookup
from mako import exceptions

from visutils import *
from common import htmlname

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Visualize set of piglit json results")
    ap.add_argument('-o','--output', help="Output html prefix", type=str, default='')
    ap.add_argument('--combine', type=str, default=None,
        help="Combine results for similar hosts")
    ap.add_argument('--testsort', default=False, action='store_true',
        help="Sort tests in alphabetical order")
    ap.add_argument('-d', '--depth', type=int, default=0,
        help="Relative dir depth of html files, added to links")
    ap.add_argument('--buildorder', type=str, default=None,
        help="Filename for build order information")
    ap.add_argument('files', help="Files to be visualized", metavar='files', nargs='+', type=str)

    args = ap.parse_args()

    relpath = "../"*args.depth

    if args.buildorder: readbuildorder(args.buildorder)

    try: jsons = readfiles(args.files, combine=args.combine) # arranged by [(build,host)]
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    tests = parsetests(jsons, sort=args.testsort)
    # keep the order same on these two arrays
    hostcols = [host for _,host in sorted(jsons, key=grouphosts)]
    buildcols = [build for build,_ in sorted(jsons, key=grouphosts)]

    rootdir = os.path.dirname(os.path.abspath(__file__))
    mako = TemplateLookup([os.path.join(rootdir, "templates"), rootdir])

    try: template = mako.get_template('history-host.mako')
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    with open(args.output+"hosts.json", 'w') as f:
        j={'hosts': [host for host in sorted(set(hostcols))]}
        f.write(json.dumps(j))

    for host in set(hostcols):
        builds = [b for b,h in sorted(jsons, key=grouphosts) if h==host]
        # Show only the tests that have changed result between builds
        hosttests=[]
        for test in tests:
            res=None
            for build in builds:
                try:
                    newres=jsons[(build,host)]['tests'][test]['result']
                except: continue
                if newres in ['notrun', 'none']: continue
                if not res: res = newres
                if res != newres:
                    hosttests.append(test)
                    break
            else:
                if res and res not in ['pass', 'skip']:
                    hosttests.append(test)

        with open(args.output+host+".html", 'w') as f:
            try:
                page = template.render(path=relpath, builds=builds, tests=hosttests, host=host, jsons=jsons)
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)

    try: template = mako.get_template('history-test.mako')
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    hosts = []
    [hosts.append(host) for host in hostcols if host not in hosts]
    for test in set(tests):
        with open(args.output+htmlname(test), "w") as f:
            try:
                page = template.render(path=relpath, builds=builds, test=test, hosts=hosts, jsons=jsons)
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)

    # get statistical information about flip-flops
    with open(args.output+"flips.txt", "w") as f:
        f10 = open(args.output+"flips10.txt", "w")
        f30 = open(args.output+"flips30.txt", "w")

        print("Flips Total Percentage Host Test", file=f)
        print("Flips Total Percentage Host Test", file=f10)
        print("Flips Total Percentage Host Test", file=f30)
        for test in set(tests):
            for host in hosts:
                results = []
                for build in builds:
                    try:
                        res = jsons[(build, host)]['tests'][test]['result']
                        if res != 'notrun': results.append(res)
                    except: # no result for this build
                        continue

                flips = 0
                total = len(results)-1
                for i in range(total):
                    if results[-1-i] != results[-2-i]: flips +=1
                if flips > 1 and total > 4: print("{f} {t} {p:4.2f} {host} {test}".format(
                    f=flips,t=total,p=100*flips/total, host=host, test=test),file=f)

                flips = 0
                total = len(results[-11:])-1 # max 10 flips in 11 results
                for i in range(total):
                    if results[-1-i] != results[-2-i]: flips += 1
                if flips > 1 and total > 4:
                    print("{f} {t} {p:4.2f} {host} {test}".format(
                        f=flips,t=total,p=100*flips/total, host=host, test=test),file=f10)

                flips = 0
                total = len(results[-31:])-1 # max 30 flips in 31 results
                for i in range(total):
                    if results[-1-i] != results[-2-i]: flips += 1
                if flips > 1 and total > 4:
                    print("{f} {t} {p:4.2f} {host} {test}".format(
                        f=flips,t=total,p=100*flips/total, host=host, test=test),file=f30)

    exit(0)
